<?php 
	try {
		$getElements 	= $conn->prepare("SELECT * FROM categories");
		$getElements->execute();
		$catElements 		= $getElements->fetchAll();
		return $catElements;
	}catch(PDOException $e){
		echo "Connection failed: " . $e->getMessage();
	}
?>