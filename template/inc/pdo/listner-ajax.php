<?php
	if(empty($limit)){
		$limit = 0;
	}
	try {
		$getElements 	= $conn->prepare("SELECT * FROM links ORDER BY id LIMIT ".$limit.",2");
		$getElements->execute();
		$elements 		= $getElements->fetchAll();
		
		if ($getElements->rowCount() > 0) {
			return $elements;
		} else {
			return false;
		}
		
	}catch(PDOException $e){
		echo "Connection failed: " . $e->getMessage();
	}
?>