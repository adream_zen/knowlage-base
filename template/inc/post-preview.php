<?php 
	$title 		= $_POST['title'];
	$categories	= $_POST['categories'];
	$href 		= $_POST['href'];
	$coment 	= $_POST['coment'];
	include 	'helpers.php';
?>

<div class="content__elem">
	<div class="content__description">
		<div class="col col--2">
			<h3>
				<?php echo $title ?>
			</h3>
			<h4>
				<a href="<?php echo $href ?>"><?php echo $href ?></a>
			</h4>
		</div>
		<div class="content__coment col col--2">
			<?php echo $categories ?>
			<?php echo $coment ?>
		</div>
	</div>

	<div class="content__embed col col--1">
	<?php 
		if (strpos($href, 'codepen') !== false) {

			get_codepen_embed($href);

		}elseif(strpos($href, 'youtube') !== false || strpos($href, 'youtu.be') !== false){
			
			get_yt_embed($href);

		}else{
			$content = get_og_image( $href );
			?>	
				<span class="image_embed" bg="<?php echo $content ?>"></span>
			<?php
		}
	?>
		
	</div>
</div>