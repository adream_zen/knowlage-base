<?php 
/**
 * Adream Theme Helpers
 *
 * @package Adream
 */

/*======================================
=            Image function            =
======================================*/

function img($src="",$class=""){
	if(!empty($src)){
		$class = !empty($class) ? $class : " ";
		echo '<img class="image '.$class.'" src="assets/img/'.$src.'" alt="'.$src.'">';
	}
}
function bgimg($src=""){
	if(!empty($src)){
		echo 'style="background-image: url(assets/img/'.$src.')"';
	}
}

/*=====  End of Image function  ======*/

/*=============================================
=            Random text generator            =
=============================================*/

if(!function_exists('random_text')){
	function random_text($max=0){
		$max_r = !empty($max) ? $max : 399;
		$lorem = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci minima, laboriosam amet. Adipisci hic minima a pariatur nam, vero, repudiandae quaerat, repellendus totam voluptates quisquam aliquam placeat est. Qui rem reprehenderit architecto minima, quidem quam, molestias eos veniam, nulla distinctio, asperiores. Harum voluptatem omnis porro quibusdam. Assumenda facilis necessitatibus, modi!";
		$rand =  rand(50, $max_r);
		echo substr($lorem, 0, $rand); 
	}
}
/*=====  End of Random text generator  ======*/


/*=========================================
=           Get page og image            =
=========================================*/

if(!function_exists('get_og_image')){
	function get_og_image($href=""){
		if(@file_get_contents($href) !== false ){
			libxml_use_internal_errors(true);
			$doc = new DomDocument();
			$doc->loadHTML(file_get_contents($href));
			$xpath = new DOMXPath($doc);
			$query = '//*/meta[starts-with(@property, \'og:\')]';
			$metas = $xpath->query($query);
			foreach ($metas as $meta) {
				$property = $meta->getAttribute('property');
				if( $property == "og:image"){
					$content = $meta->getAttribute('content');
				}						
			}
		}else{
			$content ="";
		}
		return @$content;
	}
}

/*=====  End of Get page og image  ======*/


/*=========================================
=            Get Codepen embed            =
=========================================*/
if (!function_exists('get_codepen_embed')) {
	function get_codepen_embed($href=""){
		if(!empty($href)){
			preg_match("/[^\/]+$/", $href, $matches);
			$last_word = $matches[0];
			$result = '<p data-height="500" data-theme-id="0" data-slug-hash="'.$last_word.'" data-default-tab="result" data-user="SitePoint" data-embed-version="2" data-pen-title="'.$href.'" class="codepen">'.$href.'<a href="'.$href.'">'.$href.'</a> by SitePoint (<a href="https://codepen.io/SitePoint">@SitePoint</a>) on <a href="https://codepen.io">CodePen</a>.</p>';
			echo $result;
		}
	}
}
/*=====  End of Get Codepen embed  ======*/

/*====================================
=            Get YT embed            =
====================================*/

if (!function_exists('get_yt_embed')) {
	function get_yt_embed($href=""){

		if(strpos($href, 'youtube') !== false){
			parse_str( parse_url($href)['query'] , $YThref);
			$YThref = $YThref['v'];
			echo '<iframe class="youtube" src="https://www.youtube.com/embed/'.$YThref.'" allowfullscreen></iframe>';
		}elseif(strpos($href, 'youtu.be') !== false){
			preg_match("/[^\/]+$/", $href, $matches);
			$YThref2 = $matches[0];			
			echo '<iframe class="youtube" src="https://www.youtube.com/embed/'.$YThref2.'" allowfullscreen></iframe>';
		}else{
			echo "error";
		}
	}
}

/*=====  End of Get YT embed  ======*/
