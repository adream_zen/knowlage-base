<?php include("view/header.php") ?>
	<main class="parent parent--content">
		<section class="row container row--content">
			<form>
				<input type="text" name="search">
				<div id="search" class="button button--search">Szukaj</div>
			</form>
		</section>
		<section class="row container row--content">
			<ul class="content__wrap"></ul>
		</section>
	</main>
	<div class="parent">
		<section class="row container row--load-more">
				<div id="load-more" class="button button--center" limit="2">POKAŻ KOLEJNE</div>
		</section>
	</div>

<?php include("view/footer.php") ?>