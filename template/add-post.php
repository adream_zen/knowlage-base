
<?php include("view/header.php") ?>
	<main class="parent parent--content">
		<section class="row container row--content">
			<div class="content__wrap preview-post">
			</div>
		</section>
		<section class="row container row--content">
			<form class="add-elem" id="form-add-post">
				<h2>Dodaj nowy wpis</h2>
				<div class="col col--2">
					<input class="add-elem__input" type="text" name="title" id="title" placeholder="Tytuł" required>
					<input class="add-elem__input" type="url" name="href" id="href" placeholder="Adres url" required>
				</div>
				<div class="col col--2">
					<select name="categories" class="add-elem__input">
						<option value="0" disabled selected>Ketegorie</option>';
						<?php 
							foreach ($catElements as $catElement) {
								echo '<option value="'.$catElement["id"].'"">'.$catElement['name'] . '</option>';
							}
						?>
					</select>
					<textarea class="add-elem__input" name="coment" id="coment" placeholder="Komentarz" required></textarea>
					<div class="button button--add" id="preview-post">Podgląd</div>
					<button class="button button--add" id="add-post">Dodaj</button>
				</div>
			</form>
		</section>
	</main>
<?php include("view/footer.php") ?>
