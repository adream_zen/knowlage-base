<?php 
	include("view/header.php");
	if($login_in!==true){
		header('Location: '.$home);
	}
	if(isset($_GET['id'])){
		$id=$_GET['id'];
	}
	include("inc/pdo/listner.php");

?>
	<main class="parent parent--content" id="edit-post">
		<section class="row container row--content">
			<div class="content__wrap preview-post">
				<?php
				if(!empty($elements)){
					foreach ($elements as $element) { ?>
						<li class="content__elem" id="post-<?php echo $element['id']?>">
							<div class="content__description">
								<div class="col col--2">
									<h3>
										<?php echo $element['title'] ?>
									</h3>
									<h4>
										<a class="href href--link" href="<?php echo $element['href'] ?>"><?php echo $element['href'] ?></a>
									</h4>
								</div>
								<div class="content__coment col col--2">
									<?php include('view/parts/category.php') ?>
									<?php echo $element['coment'] ?>
								</div>
							</div>
							<div class="content__embed col col--1">
							<?php 
								if (strpos($element['href'], 'codepen') !== false) {

									get_codepen_embed($element['href']);

								}elseif(strpos($element['href'], 'youtube') !== false || strpos($element['href'], 'youtu.be') !== false){
									
									get_yt_embed($element['href']);

								}else{
									$content = get_og_image( $element['href'] );
									?>	
										<span class="image_embed" bg="<?php echo $content ?>"></span>
									<?php
								}
							?>
								
							</div>
						</li>
				<?php }	}?>
			</div>
		</section>
		<section class="row container row--content">
			<form class="add-elem" id="form-edit-post">
				<h2>Edytuj wpis</h2>
				<div class="col col--2">
					<input type="hidden" name="id" required value="<?php echo $elements[0]['id'] ?>">
					<input class="add-elem__input" type="text" name="title" id="title" placeholder="Tytuł" required value="<?php echo $elements[0]['title'] ?>">
					<input class="add-elem__input" type="url" name="href" id="href" placeholder="Adres url" required value="<?php echo $elements[0]['href'] ?>">
				</div>
				<div class="col col--2">
					<select name="categories" class="add-elem__input">
						<option disabled>Kategorie</option>
						<?php 
							foreach ($catElements as $catElement) {
								if($catElement['id']==$element['id'] ){
									echo '<option value='.$catElement["id"].' selected>'.$catElement['name'] . '</option>';
								}else{
									echo '<option value='.$catElement["id"].'>'.$catElement['name'] . '</option>';
								}
							}
						?>
					</select>
					<textarea class="add-elem__input" name="coment" id="coment" placeholder="Komentarz" required><?php echo $elements[0]['coment'] ?></textarea>
					<div class="button button--add" id="preview-post">Podgląd</div>
					<button class="button button--add" id="save-post">Zapisz</button>
				</div>
			</form>
		</section>
	</main>
<?php include("view/footer.php") ?>
