<?php
	if(!empty($_POST['search'])){
		$param = $_POST['search'];
	}
	include("../../inc/base.php");
	include("../../inc/pdo/connect.php");
	include("../../inc/pdo/search-listner.php");
	include("../../inc/helpers.php");
	include("../../inc/session.php");
	include("../../inc/pdo/categories-listner.php");
	
	if(!empty($elements)){

		foreach ($elements as $element) { ?>
			<li class="content__elem" id="post-<?php echo $element['id']?>">
				<div class="content__description">
					<div class="col col--2">
						<h3>
							<?php echo $element['title'] ?>
						</h3>
						<?php include('category.php') ?>
						<h4>
							<a target="_blank" class="href href--link" href="<?php echo $element['href'] ?>"><?php echo $element['href'] ?></a>
						</h4>
					</div>
					<div class="content__coment col col--2">
						<?php echo $element['coment'] ?>
					</div>
				</div>
				<div class="content__embed col col--1">
				<?php 
					if (strpos($element['href'], 'codepen') !== false) {

						get_codepen_embed($element['href']);

					}elseif(strpos($element['href'], 'youtube') !== false || strpos($element['href'], 'youtu.be') !== false){
						
						get_yt_embed($element['href']);

					}else{
						$content = get_og_image( $element['href'] );
						?>	
							<span class="image_embed" bg="<?php echo $content ?>"></span>
						<?php
					}
				?>
					
				</div>
				<?php if($login_in === true){ ?>
					<div class="col col--1">
						<a href="edit-post.php?id=<?php echo $element['id'] ?>" class="button button--edit">Edycja</a>
						<div post-id="<?php echo $element['id'] ?>" class="button button--remove remove-post">Usuń wpis</div>
					</div>
				<?php } ?>
			</li>
<?php	
		}
	}else{
		return false;
	}
?>