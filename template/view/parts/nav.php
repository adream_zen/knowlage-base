<input type="checkbox" id="m_nav">
<label for="m_nav" id="arrow" onclick>
	<span></span>
	<span></span>
</label>
<nav>
	<ul>
		<li>
			<a class="ico ico--home" href="<?php echo $home ?> ">HOME</a>
		</li>
		<li>
			<a class="ico ico--search" href="search.php">Szukaj</a>
		</li>
		<?php if($login_in===true){ ?>
			<li><a class="ico ico--add" href="add-post.php">DODAJ WPIS</a></li>
			<li><a class="ico ico--logout" href="logout.php">WYLOGUJ</a></li>
		<?php }else{ ?>
			<li><a class="ico ico--login" href="login.php">LOGOWANIE</a></li>
		<?php } ?>
	</ul>
</nav>