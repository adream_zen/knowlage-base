<?php
/**
 *
 * @package Adream
 */

?>
	<footer class="parent parent--footer">
		<section class="row container row--footer">
			<div class="col col--2">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ullam.
			</div>
			<div class="col col--2">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam delectus error corporis dolore fugiat quam adipisci, officiis natus temporibus inventore!
			</div>
		</section>
	</footer>
	<div class="parent parent--copyright" >
		<section class="row container row--copyright">
			<div class="col col--2">&copy;<?php echo date("Y"); echo ' | '; echo $title; ?></div>
			<div class="col col--2">
				<span>Projekt i realizacja</span> <a href="zensite.pl"><?php img("zensite.png","zensite") ?></a>
			</div>
		</section>
	</div>
</div>
<link rel="stylesheet" id="adream-scss-css" href="assets/styles/style.css" type="text/css" media="all">
<script src="assets/scripts/scripts.js" async></script>
<!-- <script async src="https://production-assets.codepen.io/assets/embed/ei.js"></script> -->
</body>
</html>

<?php $conn = null; ?>