import $				from 'jquery';

// import tabs 			from './tabs';
// import scrollTop 	from './scrollTop';
// import userScroll 	from './userScroll';
// import achorNav 		from './achorNav';
// import parallax 		from './parallax';
// import cokies 		from './cokies';
// import wordSizer 	from './wordSizer';
// import gallery		from './gallery';
import codepen			from './codepen.js';

function Images(){
	$(".image_embed").each(function(){
		var bgSrc = $(this).attr("bg");
		if( bgSrc != '' ){
			$(this).html('<img src="'+bgSrc+'">');
			$(this).find("img").on("load",function(){
				$(this).css("opacity","1");
			})
		}
	})
}
function loadContet(limit){
	$.ajax({    //create an ajax request to load_page.php
		type: "POST",
		url: "view/parts/index-listner.php",
		data: 'limit='+limit,
		dataType: "html",   //expect html to be returned
		success: function(response){
			if(response != ''){
				$(".content__wrap").append(response);
				// alert(response);
				new codepen();
				Images();
				var rageLimit = limit + 2;
				$("#load-more").attr("limit",rageLimit);
			}
		}//,
	});
}

function searchContet(param){
	$.ajax({    //create an ajax request to load_page.php
		type: "POST",
		url: "view/parts/search-listner.php",
		data: 'search='+param,
		dataType: "html",   //expect html to be returned
		success: function(response){
			if(response != ''){
				$(".content__wrap").append(response);
				// alert(response);
				new codepen();
				Images();
				var rageLimit = limit + 2;
				$("#load-more").attr("limit",rageLimit);
			}
		}
	});
}

function Suc(data){
	$("#alerts").addClass("success");
	$("#alerts").html(data);
	setTimeout(function(){
		$("#alerts").removeClass('success');
	}, 1500);
}
function Err(data){
	$("#alerts").addClass("error");
	$("#alerts").html(data);
}

/*================================
=            Add post            =
================================*/

$(document).ready(function(){
	var form = $("#form-add-post");
	form.submit(function( event ) {
		$.ajax({
			type: "POST",
			url: 'inc/pdo/add-post.php',
			data: form.serialize(), // serializes the form's elements.
			success: function(data){
				Suc(data);
			},
			error: function(data){
				Err(data);
			}
		})
		event.preventDefault();
	})
})

/*=====  End of Add post  ======*/

/*=================================
=            Edit post            =
=================================*/

$(document).ready(function(){
	var form = $("#form-edit-post");
	form.submit(function( event ) {
		$.ajax({
			type: "POST",
			url: 'inc/pdo/edit-post.php',
			data: form.serialize(), // serializes the form's elements.
			success: function(data){
				Suc(data);
				location.reload();
			},
			error: function(data){
				Err(data);
			}
		})
		event.preventDefault();
	})
})

/*=====  End of Edit post  ======*/


/*====================================
=            Post Preview            =
====================================*/

$(document).ready(function(){
	var form = $("#form-edit-post,#form-add-post");
	$("#preview-post").click(function(){
		$(".content__wrap").empty();
		$(".content__wrap").load("inc/post-preview.php",{
			title: 		form.find('input[name="title"]').val(),
			categories: form.find('select[name="categories"] option:selected').text(),
			href: 		form.find('input[name="href"]').val(),
			coment: 	form.find('textarea[name="coment"]').val()
		},function(){
			Images();
			new codepen();
		});
	})
})

/*=====  End of Post Preview  ======*/

/*===================================
=            Remove Post            =
===================================*/

$(()=>{
	$(document).on("click",".remove-post",function(){
		var post_id = $(this).attr("post-id");
		$.ajax({
			type: "POST",
			url: 'inc/pdo/remove-post.php',
			data: 'id='+post_id,
			success: function(data){
				$("#post-"+post_id).remove();
				Suc(data);
			},
			error: function(data){
				Err(data);
			}
		})
	})
})

/*=====  End of Remove Post  ======*/

$(()=>{
	$("#alerts").click(function(){
		$(this).removeClass("success");
		$(this).removeClass("error");
	})
});

/*==========================================
=            Load index elemets            =
==========================================*/

if($("body").hasClass("home") ){
	loadContet(0);
}
if( $("main").is("#edit-post") ){
	new codepen();
	Images();
}

$(()=>{
	$(document).on("click","#load-more:not(.disable)",function(){
		$("#load-more").addClass("disable");
		// alert();
		var limit = parseInt($(this).attr("limit"));
		loadContet(limit);
	})
});
$(document).bind("ajaxStart",function(){
	$("#load-more").addClass("disable");
})
$(document).bind("ajaxStop",function(){
	$("#load-more").removeClass("disable");
})

/*=====  End of Load index elemets  ======*/

$(()=>{
	$(document).on("click","#search:not(.disable)",function(){
		// $("#load-more").addClass("disable");
		// alert();
		var param = $('input[name="search"]').val();
		searchContet(param);
	})
});