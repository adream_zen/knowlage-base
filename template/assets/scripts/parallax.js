import $	from 'jquery';

export default function(selector) {
	if( $(window).width() >  576){
		if(selector.length){
			$(window).scroll(function(){
				var speed = 0.3;
				var scrollTop = $(window).scrollTop();
				var pos = scrollTop - $(selector).offset().top;
				$(selector).css("background-position","center calc(40% + " + pos*speed +"px)");
			})
		}
	}
}